package net.wilj.challenge.countries.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class State {
    @NotBlank(message = "Country code is required.")
    private String countryCode;
    @NotBlank(message = "State code is required.")
    private String stateCode;
    @NotBlank(message = "State name is required.")
    private String stateName;
    @NotNull(message = "Population is required")
    @Min(value = 0, message = "Population cannot be negative.")
    @Max(value = Integer.MAX_VALUE, message = "Population must be less than " + Integer.MAX_VALUE)
    private Integer population;
}