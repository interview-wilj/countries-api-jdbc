create schema challenge;

create table challenge.countries (
    code            text primary key,
    name            text not null,
    currency        text not null
);

create table challenge.states (
    country_code    text not null,
    code            text not null,
    name            text not null,
    population      integer,
    primary key (country_code, code),
    constraint fk_countries
        foreign key(country_code)
        references countries(code)
);


create temporary table challenge_input (
    id serial primary key,
    country         text,
    country_code    text,
    currency        text,
    state           text,
    state_code      text,
    population      integer
);

-- load the data into a temporary table for pre-processing
COPY challenge_input (country, country_code, currency, state, state_code, population)
FROM '/docker-entrypoint-initdb.d/data.csv'
DELIMITER ','
CSV HEADER;


-- fill in the missing country_code values
do
$$
declare
    r record;
    last_country_code text := '';
begin
    for r in 
        select id, country_code 
        from challenge_input 
        order by id
    loop
        if coalesce(r.country_code, last_country_code) <> last_country_code then
            -- raise notice 'country_code changed: %s', last_country_code;
            last_country_code := upper(r.country_code);
        end if;
        if r.country_code is null or r.country_code <> last_country_code then
            -- raise notice 'updating country code %s', last_country_code;
            update challenge_input
            set country_code = last_country_code
            where id = r.id;
        end if;

end loop;
end;
$$;

insert into challenge.countries (code, name, currency)
    select upper(country_code), max(country), max(currency)
    from challenge_input
    where country_code is not null
    group by upper(country_code);

insert into challenge.states (country_code, code, name, population)
    select upper(country_code), upper(state_code), state, population
    from challenge_input
    order by id;
