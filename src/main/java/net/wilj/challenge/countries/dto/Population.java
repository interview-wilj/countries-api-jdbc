package net.wilj.challenge.countries.dto;

import lombok.Data;

@Data
public class Population {
    private String code;
    private Integer population;
}
