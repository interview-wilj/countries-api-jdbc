package net.wilj.challenge.countries;

import net.wilj.challenge.countries.dto.State;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping("/api/countries")
public class CountriesController {


    final CountriesRepository countries;
    public CountriesController(final CountriesRepository countries) {
        this.countries = countries;
    }

    @GetMapping("/populations")
    public ResponseEntity<?> getAllCountryPopulations() {
        Map<String, Integer> result = new TreeMap<>();
        countries.getAllCountryPopulations().forEach(c -> result.put(c.getCode(), c.getPopulation()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/currencies")
    public ResponseEntity<?> getAllCountryCurrencies() {
        Map<String, String> result = new TreeMap<>();
        countries.getAllCountryCurrencies().forEach(c -> result.put(c.getCode(), c.getCurrency()));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{countryCode}/states/{stateCode}/validate")
    public boolean validateState(@PathVariable("countryCode") String countryCode, @PathVariable("stateCode") String stateCode) {
        return countries.validateState(countryCode, stateCode);
    }

    @PutMapping("/{countryCode}/states/{stateCode}")
    public State addState(@PathVariable("countryCode") String countryCode, @PathVariable("stateCode") String stateCode, @Valid @RequestBody State state) {
        if (countryCode.equalsIgnoreCase(state.getCountryCode()) && stateCode.equalsIgnoreCase(state.getStateCode())) {
            return countries.addState(state.getCountryCode(), state.getStateCode(), state.getStateName(), state.getPopulation());
        } else {
            throw new IllegalArgumentException("Path parameters must match request body.");
        }
    }

}