package net.wilj.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
class ChallengeApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllCountryPopulations() throws Exception {
        this.mockMvc.perform(get("/api/countries/populations")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(hasJsonPath("$.BLD")))
                .andExpect(content().string(hasJsonPath("$.MDR")))
                .andExpect(content().string(hasJsonPath("$.NUM")))
                .andDo(document("getAllCountryPopulations"));
    }

    @Test
    public void getAllCountryCurrencies() throws Exception {
        this.mockMvc.perform(get("/api/countries/currencies")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(hasJsonPath("$.BLD", equalTo("CHK"))))
                .andExpect(content().string(hasJsonPath("$.MDR", equalTo("GUL"))))
                .andExpect(content().string(hasJsonPath("$.NUM", equalTo("DIG"))))
                .andDo(document("getAllCountryCurrencies"));
    }


    @Test
    public void validateState() throws Exception {
        this.mockMvc.perform(get("/api/countries/NUM/states/ONE/validate")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("true")))
                .andDo(document("validateState"));

        this.mockMvc.perform(get("/api/countries/FAIL/states/FAIL/validate")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("false")));
    }


    private ResultActions addState(final String countryCode, final String stateCode, final String stateName, final Integer population) throws Exception {
        return this.mockMvc.perform(put("/api/countries/%s/states/%s".formatted(countryCode, stateCode))
                .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                .content("""
                               {
                                "countryCode": "%s",
                                "stateCode": "%s",
                                "stateName": "%s",
                                "population": %d
                               }
                        """.formatted(countryCode, stateCode, stateName, population)
                )
        );
    }

    private ResultActions addStateAndVerify(final String countryCode, final String stateCode, final String stateName, final Integer population) throws Exception {
        return addState(countryCode, stateCode, stateName, population).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(hasJsonPath("$.countryCode", equalTo(countryCode.toUpperCase()))))
                .andExpect(content().string(hasJsonPath("$.stateCode", equalTo(stateCode.toUpperCase()))))
                .andExpect(content().string(hasJsonPath("$.stateName", equalTo(stateName))))
                .andExpect(content().string(hasJsonPath("$.population", equalTo(population))));
    }
    @Test
    public void addStates() throws Exception {
        addStateAndVerify("BLD", "NEW", "New State", 123456)
                .andDo(document("addState"));

        addStateAndVerify("bld", "lower", "Lower State", 54321);

        final String[] codes = {"NIN", "TEN", "ELV", "TWE"};
        final String[] names = {"Nine", "Ten", "Eleven", "Twelve"};

        for (int i = 0; i < codes.length; i++) {
            addStateAndVerify("NUM", codes[i], names[i], 999 * (i + 9));
        }
    }

    @Test
    public void addStatesValidation() throws Exception {
        addState("BLD", "FAIL", "", 1)
                .andExpect(status().is4xxClientError());
        addState("BLD", "FAIL", "FAIL", -1)
                .andExpect(status().is4xxClientError());
    }


}
