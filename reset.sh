#!/usr/bin/env bash
set -euxo pipefail

docker rm $(docker ps -aq) || echo "no containers to stop"

docker volume rm $(docker volume ls -q) || echo "no volumes to delete"

docker-compose -f postgresql.yml up