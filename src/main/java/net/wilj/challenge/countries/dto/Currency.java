package net.wilj.challenge.countries.dto;

import lombok.Data;

@Data
public class Currency {
    private String code;
    private String currency;
}

