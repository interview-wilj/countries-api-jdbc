package net.wilj.challenge.countries;

import net.wilj.challenge.countries.dto.State;
import net.wilj.challenge.countries.dto.Currency;
import net.wilj.challenge.countries.dto.Population;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountriesRepository extends Repository<State, String> {

    /**
     * @return all of the countries with the currency of each country.
     */
    @Query("""
        select
            code,
            currency
        from countries
    """)
    List<Currency> getAllCountryCurrencies();

    /**
     * @return all of the countries with the total populations.
     */
    @Query("""
        select
            country_code code,
            sum(population) population
        from states
        group by country_code
    """)
    List<Population> getAllCountryPopulations();

    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    @Query("""
        select
            case
                when count(s.*) > 0 then true
                else false
            end
        from states s
        where s.country_code = :countryCode
            and s.code = :stateCode
    """)
    boolean validateState(@Param("countryCode") String countryCode, @Param("stateCode") String stateCode);


    /**
     * @return a boolean indicating whether or not the supplied county code and state code combination are valid.
     */
    @Query("""
        insert into states (country_code, code, name, population)
        values (upper(:countryCode), upper(:code), :name, :population)
            on conflict (country_code, code)
            do update set
                name = :name,
                population = :population
            returning country_code, code as state_code, name as state_name, population
    """)
    State addState(
            @Param("countryCode") String countryCode,
            @Param("code") String code,
            @Param("name") String name,
            @Param("population") Integer population
            );


}