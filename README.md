# Java coding challenge

This is the non-JPA version of the Java coding challenge described below.

## [Click here to open the project in a workspace on Gitpod.io](https://gitpod.io/#https://gitlab.com/interview-wilj/countries-api-jdbc)

![Gitpod.io workspace example](docs/gitpod-screenshot.png)

## Local development instructions

### Populating the database

The database population scripts are in `src/main/sql` and are comprised of a SQL file containing the schema definition,
and a CSV file containing the data to load.

Any of the following 3 methods for launching the database should result in a populated PostgreSQL database listening on localhost:5432.

**Note:** Commands must be run from the project directory.

#### Docker
```shell
    docker run \
      -e "POSTGRES_USER=challenge" \
      -e "POSTGRES_PASSWORD=" \
      -e "POSTGRES_HOST_AUTH_METHOD=trust" \
      -v "/vagrant/src/main/sql:/docker-entrypoint-initdb.d:ro" \
      -p "127.0.0.1:5432:5432" \
        docker.io/library/postgres:14.2
```

#### docker-compose
```shell
  docker-compose -f postgresql.yml up
```

#### Vagrant + Virtualbox
```shell
  vagrant up  
```

---

### Building and running the application

#### IntelliJ IDEA

The project includes run configurations for IntelliJ IDEA:
* challenge-test - runs unit tests and generates API documentation
* challenge-package - builds the executable JAR file
* challenge-run - runs the application

#### Maven

```shell
mvn clean install spring-boot:repackage
cd target
java -Dspring.datasource.url=jdbc:postgresql://localhost:5432/challenge -Dspring.datasource.username=challenge -jar challenge-0.0.1-SNAPSHOT.jar
```

---

## Original challenge requirements

Create a database of your desired flavor that can store simple data based on the following fictional country and state information:

<div id="data">

| Country     | Country Code  | Currency  | State            | State Code  | State Population  |
|-------------|---------------|-----------|------------------|-------------|-------------------|
| Big Land    | BLD           | CHK       | Left Province    | LEF         | 10,100            |
|             |               |           | Right Province   | RIG         | 778,030           |
|             |               |           | Topside          | TSD         | 2,200,340         |
|             |               |           | Center Province  | CTR         | 1,340,922         |
| Mordor      | MDR           | GUL       | Udun             | UDN         | 2,000,110         |
|             |               |           | Gorgoroth        | GOR         | 3,120,900         |
|             |               |           | Nurn             | NRN         | 1,100,000         |
|             |               |           | Kand             | KND         | 2,500,000         |
| Numberland  | NUM           | DIG       | One              | ONE         | 1,1500,00         |
|             |               |           | Two              | TWO         | 25,320,000        |
|             |               |           | Three            | TRE         | 3,100,00          |
|             |               |           | Four             | FOR         | 400,000           |

</div>

Create the following web services to access this data:

1.  getAllCountryPopulations()

    - GET method that returns all of the countries with the total populations of each country as a JSON object keyed on country code

1.  getAllCountryCurrencies()

    - GET method that returns all of the countries with the currency of each country as a JSON object keyed on country code

1.  validateState(country\_code, state\_code)

    - GET method that returns a boolean indicating whether or not the supplied county code and state code combination are valid

1.  addState(country\_cd, state\_cd, state\_name, state\_population)

    - PUT method that will add the supplied state information to the database above

Please implement your solution in Java and send us the following:

1.  Source code for your solution to the above requests
2.  Schema of the database defined to hold the above data
3.  The mechanism used to populate the initial values in the database (hint: don’t hard code these entries)
4.  WAR file or Spring Boot Jar we can run in tomcat to test your code

If you have any questions, please use your best judgment and document any assumptions you have made.